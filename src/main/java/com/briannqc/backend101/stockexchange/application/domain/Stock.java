package com.briannqc.backend101.stockexchange.application.domain;

public class Stock {

	private final String symbol;

	private final String company;

	private Stock(String symbol, String company) {
		this.symbol = symbol;
		this.company = company;
	}

	/**
	 * Creates a Stock domain entity of symbol and company name.
	 *
	 * @param symbol  Consists of 4 uppercase characters.
	 * @param company Not blank string without heading nor tailing whitespaces.
	 * @throws IllegalArgumentException - If any of the input violates the rules.
	 */
	public static Stock newStock(final String symbol, final String company) {
		final var symbolWithoutWhiteSpace = removeAllWhitespaces(symbol);
		if (symbolWithoutWhiteSpace.length() != 4) {
			throw new IllegalArgumentException("symbol must have 4 uppercase characters");
		}
		if (!symbolWithoutWhiteSpace.toUpperCase().equals(symbol)) {
			throw new IllegalArgumentException("symbol must have 4 uppercase characters");
		}

		if (!company.trim().equals(company)) {
			throw new IllegalArgumentException("company name must not be blank");
		}
		return new Stock(symbol, company);
	}

	private static String removeAllWhitespaces(String s) {
		return s.replaceAll("\\s+", "");
	}

	public String getSymbol() {
		return symbol;
	}

	public String getCompany() {
		return company;
	}
}
