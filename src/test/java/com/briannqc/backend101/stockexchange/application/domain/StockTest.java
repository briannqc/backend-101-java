package com.briannqc.backend101.stockexchange.application.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StockTest {

	@ParameterizedTest(name = "{0}")
	@MethodSource
	void createStock(
			final String ignoreTestName,
			final String symbol,
			final String company,
			final boolean shouldSucceed
	) {
		Stock actualStock = null;
		Exception actualException = null;

		try {
			actualStock = Stock.newStock(symbol, company);
		} catch (final Exception ex) {
			actualException = ex;
		}

		if (shouldSucceed) {
			assertThat(actualException).isNull();
			assertThat(actualStock.getSymbol()).isEqualTo(symbol);
			assertThat(actualStock.getCompany()).isEqualTo(company);
		} else {
			assertThat(actualException).isNotNull().isInstanceOf(IllegalArgumentException.class);
		}
	}

	private static List<Arguments> createStock() {
		return List.of(
				Arguments.of(
						"GIVEN company name is blank WHEN create stock THEN fails",
						"AAPL",
						"\t\t    \t\t",
						false
				),
				Arguments.of(
						"GIVEN company name is not blank WHEN create stock THEN passes",
						"AAPL",
						"Apple Inc.",
						true
				),
				Arguments.of(
						"GIVEN company name starts with whitespaces WHEN create stock THEN fails",
						"AAPL",
						"      Apple Inc.",
						false
				),
				Arguments.of(
						"GIVEN company name ends with whitespaces WHEN create stock THEN fails",
						"AAPL",
						"Apple Inc.   ",
						false
				),
				Arguments.of(
						"GIVEN symbol has less than 4 chars WHEN create stock THEN fails",
						"APL",
						"Apple Inc.",
						false
				),
				Arguments.of(
						"GIVEN symbol has more than 4 chars WHEN create stock THEN fails",
						"APPLE",
						"Apple Inc.",
						false
				),
				Arguments.of(
						"GIVEN symbol has lowercase chars WHEN create stock THEN fails",
						"Appl",
						"Apple Inc.",
						false
				),
				Arguments.of(
						"GIVEN symbol has whitespace chars WHEN create stock THEN fails",
						"A PL",
						"Apple Inc.",
						false
				),
				Arguments.of(
						"GIVEN symbol has 4 uppercase chars WHEN create stock THEN passes",
						"AAPL",
						"Apple Inc.",
						true
				)
		);
	}
}
