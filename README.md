# Backend 101 Java

This is the companion repository of the Backend 101 blog series using Java 17. There is another repo using Golang
which serves the same purpose at: https://gitlab.com/briannqc/backend-101.

Checkout the blog series on [Medium](https://medium.com/@briannqc/backend-101-introduction-2e3c2ef448).
